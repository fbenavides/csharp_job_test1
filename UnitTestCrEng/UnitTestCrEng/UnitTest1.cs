﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CREng.classes;

namespace UnitTestCrEng
{
    [TestClass]
    public class UnitTestCrEngTest
    {
        [TestMethod]
        public void TestAddOneCourseToCollection()
        {
            Courses courses = new Courses();

            courses.add("Course_one");

            Assert.AreEqual(1, courses.count );
        }

        [TestMethod]
        public void TestNotAddSameCourseToCollection()
        {
            Courses courses = new Courses();

            courses.add("Course_one");
            courses.add("Course_one");

            Assert.AreEqual(1, courses.count);
        }

        [TestMethod]
        public void TestAddCourseToCollectionWithPrerequisite()
        {
            Courses courses = new Courses();

            courses.add("Course one", "prerequisite one");

            Assert.AreEqual(2, courses.count);

        }

        [TestMethod]
        public void TestValidOutputTestOnePrerequisite()
        {
            Courses courses = new Courses();

            courses.add("Course one", "prerequisite one");

            Assert.AreEqual(2, courses.count);
            Assert.AreEqual("prerequisite one, Course one", courses.getOutput());
        }

        [TestMethod]
        public void TestValidOutputTestOnePrerequisiteVersionTwo()
        {
            Courses courses = new Courses();

            courses.add("Course one", "prerequisite one");
            courses.add("prerequisite one");

            Assert.AreEqual(2, courses.count);
            Assert.AreEqual("prerequisite one, Course one", courses.getOutput());
        }

        [TestMethod]
        public void TestValidOutputTestOnePrerequisiteVersionThree()
        {
            Courses courses = new Courses();

            courses.add("prerequisite one");
            courses.add("Course one", "prerequisite one");

            Assert.AreEqual(2, courses.count);
            Assert.AreEqual("prerequisite one, Course one", courses.getOutput());
        }

        [TestMethod]
        public void TestValidOutputTestFourCourses()
        {
            Courses courses = new Courses();

            courses.add("Introduction to Paper Airplanes");
            courses.add("Advanced Throwing Techniques", "Introduction to Paper Airplanes");
            courses.add("History of Cubicle Siege Engines", "Advanced Throwing Techniques");

            Assert.AreEqual(3, courses.count);
            Assert.AreEqual("Introduction to Paper Airplanes, Advanced Throwing Techniques, History of Cubicle Siege Engines", courses.getOutput());
        }

        [TestMethod]
        public void TestValidOutputTestFourCoursesVersionTwo()
        {
            Courses courses = new Courses();

            courses.add("Advanced Throwing Techniques", "Introduction to Paper Airplanes");
            courses.add("History of Cubicle Siege Engines", "Advanced Throwing Techniques");
            courses.add("Introduction to Paper Airplanes");

            Assert.AreEqual(3, courses.count);
            Assert.AreEqual("Introduction to Paper Airplanes, Advanced Throwing Techniques, History of Cubicle Siege Engines", courses.getOutput());
        }

        [TestMethod]
        public void TestValidOutputTestFourCoursesVersionThree()
        {
            Courses courses = new Courses();

            courses.add("History of Cubicle Siege Engines", "Advanced Throwing Techniques");
            courses.add("Advanced Throwing Techniques", "Introduction to Paper Airplanes");
            courses.add("Introduction to Paper Airplanes");

            Assert.AreEqual(3, courses.count);
            Assert.AreEqual("Introduction to Paper Airplanes, Advanced Throwing Techniques, History of Cubicle Siege Engines", courses.getOutput());
        }

        [TestMethod]
        public void TestValidOutputTestFourCoursesVersionFour()
        {
            Courses courses = new Courses();

            courses.add("History of Cubicle Siege Engines", "Advanced Throwing Techniques");
            courses.add("Introduction to Paper Airplanes");
            courses.add("Advanced Throwing Techniques", "Introduction to Paper Airplanes");

            Assert.AreEqual(3, courses.count);
            Assert.AreEqual("Introduction to Paper Airplanes, Advanced Throwing Techniques, History of Cubicle Siege Engines", courses.getOutput());
        }

        [TestMethod]
        public void TestValidOutputFive()
        {
            Courses courses = new Courses();

            courses.add("Introduction to Paper Airplanes");
            courses.add("Advanced Throwing Techniques", "Introduction to Paper Airplanes");
            courses.add("History of Cubicle Siege Engines", "Rubber Band Catapults 101");
            courses.add("Advanced Office Warfare", "History of Cubicle Siege Engines");
            courses.add("Rubber Band Catapults 101");
            courses.add("Paper Jet Engines", "Introduction to Paper Airplanes");

            Assert.AreEqual(6, courses.count);
            Assert.AreEqual("Introduction to Paper Airplanes, Rubber Band Catapults 101, Advanced Throwing Techniques, Paper Jet Engines, History of Cubicle Siege Engines, Advanced Office Warfare", 
                courses.getOutput());
        }

        [TestMethod]
        public void TestValidOutputSix()
        {
            Courses courses = new Courses();
            
            courses.add("Advanced Throwing Techniques", "Introduction to Paper Airplanes");
            courses.add("History of Cubicle Siege Engines", "Rubber Band Catapults 101");
            courses.add("Advanced Office Warfare", "History of Cubicle Siege Engines");
            courses.add("Rubber Band Catapults 101");
            courses.add("Paper Jet Engines", "Introduction to Paper Airplanes");
            courses.add("Introduction to Paper Airplanes");

            Assert.AreEqual(6, courses.count);
            Assert.AreEqual("Introduction to Paper Airplanes, Rubber Band Catapults 101, Advanced Throwing Techniques, Paper Jet Engines, History of Cubicle Siege Engines, Advanced Office Warfare",
                courses.getOutput());
        }

        [TestMethod]
        public void TestValidOutputSeven()
        {
            Courses courses = new Courses();

            courses.add("Rubber Band Catapults 101");
            courses.add("Advanced Throwing Techniques", "Introduction to Paper Airplanes");
            courses.add("History of Cubicle Siege Engines", "Rubber Band Catapults 101");
            courses.add("Advanced Office Warfare", "History of Cubicle Siege Engines");
            courses.add("Paper Jet Engines", "Introduction to Paper Airplanes");
            courses.add("Introduction to Paper Airplanes");

            Assert.AreEqual(6, courses.count);
            Assert.AreEqual("Rubber Band Catapults 101, Introduction to Paper Airplanes, History of Cubicle Siege Engines, Advanced Throwing Techniques, Paper Jet Engines, Advanced Office Warfare",
                courses.getOutput());
        }

        [TestMethod]
        public void TestValidOutputEight()
        {
            Courses courses = new Courses();

            courses.add("Advanced Office Warfare", "History of Cubicle Siege Engines");
            courses.add("Advanced Throwing Techniques", "Introduction to Paper Airplanes");
            courses.add("History of Cubicle Siege Engines", "Rubber Band Catapults 101");
            courses.add("Paper Jet Engines", "Introduction to Paper Airplanes");
            courses.add("Introduction to Paper Airplanes");
            courses.add("Rubber Band Catapults 101");

            Assert.AreEqual(6, courses.count);
            Assert.AreEqual("Introduction to Paper Airplanes, Rubber Band Catapults 101, Advanced Throwing Techniques, Paper Jet Engines, History of Cubicle Siege Engines, Advanced Office Warfare",
                courses.getOutput());
        }

        [TestMethod]
        public void TestInValidInput()
        {
            Courses courses = new Courses();

            courses.add("Intro to Arguing on the Internet", "Godwin’s Law");
            courses.add("Understanding Circular Logic", "Intro to Arguing on the Internet");
            courses.add("Godwin’s Law", "Understanding Circular Logic");

            Assert.AreEqual(3, courses.count);
            Assert.AreEqual("Invalid input", courses.getOutput());
        }

    }
}
