﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREng.classes
{
    public class Courses
    {
        public int count
        {
            get { return courses_collection.Count; }
        }
        private int countPrerequisite;
        private List<Course> courses_collection = new List<Course>();

        public Courses()
        {
            this.countPrerequisite = 0;
        }

        public void add(string courseName, string prerequisiteName = "")
        {
            Course courseFound = courses_collection.Find(x => x.name == courseName);
            if (null == courseFound)
            {
                courseFound = new Course(courseName, false, ("" != prerequisiteName));
                courses_collection.Add(courseFound);
            }
            if ("" != prerequisiteName)
            {
                courseFound.prerequisite = this.addPrerequisite(prerequisiteName);
                courseFound.hasPrerequisite = true;
            }
        }

        private Course addPrerequisite(String prerequisiteName)
        {
            Course courseFound = courses_collection.Find(x => x.name == prerequisiteName);
            if (null == courseFound)
            {
                courseFound = new Course(prerequisiteName, true, false);

                courses_collection.Add(courseFound);
                this.countPrerequisite++;
            }
            courseFound.isPrerequisite = true;
            return courseFound;
        }

        public string getOutput()
        {
            string output = "";

            List<Course> courseList;
            courseList = courses_collection.FindAll(x => x.isPrerequisite == true);
            if (courseList.Count == this.count)
                return "Invalid input";

            courseList = courses_collection.FindAll(x => x.hasPrerequisite == false);

            do
            {
                foreach (Course courseRequisite in courseList)
                {
                    output += CoursePrintName(courseRequisite);
                }

                courseList = getOutputRest(courseList);
            } while (courseList.Count > 0);

            return output.Remove(output.Length - 2);
        }

        private List<Course> getOutputRest(List<Course> courseList)
        {
            List<Course> ChildCourseList = new List<Course>();

            foreach (Course courseRequisite in courseList)
            {
                ChildCourseList.AddRange(courses_collection.FindAll(x => x.prerequisite == courseRequisite));
            }

            return ChildCourseList;
        }

        private string CoursePrintName( Course course )
        {
            return course.PrintName();
        }
    }
}
