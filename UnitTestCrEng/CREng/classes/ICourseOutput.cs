﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREng.classes
{
    public interface ICourseOutput
    {
        string Output();
    }
}
