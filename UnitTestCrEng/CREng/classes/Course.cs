﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREng.classes
{
    public class Course
    {
        public  string  name;
        public  Course  prerequisite;
        private Boolean printed       = false;
        public  Boolean isPrerequisite { get; set; }
        public  Boolean hasPrerequisite { get; set; }

        public Course(string name, Boolean isPrerequisite, Boolean hasPrerequisite)
        {
            this.name            = name;
            this.isPrerequisite  = isPrerequisite;
            this.hasPrerequisite = hasPrerequisite;
        }

        public Course(string name, Course prerequisite)
        {
            this.name         = name;
            this.prerequisite = prerequisite;
            //this.hasRequisite = true;
        }

        public string PrintName()
        {
            if (this.printed)
                return "";
            this.printed = true;
            return this.name + ", ";
        }


        
    }
}
