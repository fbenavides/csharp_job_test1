﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREng.classes
{
    public class CourseOutputString: ICourseOutput
    {
        private Course course;

        public CourseOutputString(Course course)
        {
            this.course = course;
        }

        public string Output()
        {
            if ( null != this.course.prerequisite )
                //return this.course.prerequisite.name + ", " + this.course.name;
                return this.course.prerequisite + ", " + this.course.name;

            return this.course.name;
        }

    }
}
