﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CREng.classes;

namespace CREng
{
    class Program
    {
        static void Main(string[] args)
        {

            const string HELP = "h";
            const string EXIT = "x";
            const string OUTPUT = ".";

            Boolean readCourse = true;
            string comando = "";
            string[] parametros;
            string[] separador = new string[] { ":" };
            Courses courses = new Courses();
            string Output = "Nothing to show.";


            Console.WriteLine("Press h for help, x for exit, . for finish the courses input."); // Prompt

            while (readCourse)
            {
                Console.WriteLine("Please enter a line indicating a Course and optional a prerequisite."); // Prompt
                comando = Console.ReadLine().ToLower(); // Get string from user
                if (EXIT == comando) // Check string
                {
                    System.Environment.Exit(1);
                }

                if (HELP == comando)
                {
                    printHelp();
                    continue;
                }

                if (OUTPUT == comando)
                {
                    break;
                }

                parametros = comando.Split(separador, StringSplitOptions.RemoveEmptyEntries);

                if ( 1 == parametros.Length)
                {
                    courses.add( parametros[0].Trim() );
                }

                if ( 2 == parametros.Length)
                {
                    courses.add(parametros[0].Trim(), parametros[1].Trim());
                }

            }

            if ( courses.count > 0 )
            {
                Output = courses.getOutput();
            }

            Console.WriteLine( Output );
            Console.ReadKey();
        }

        static void printHelp()
        {
            Console.WriteLine("Examples:");
            Console.WriteLine("Couser A:");
            Console.WriteLine("Couser B: Course A");
        }
    }
}
